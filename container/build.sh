#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

IMAGE_TAGE="${IMAGE_TAGE:-"enviteconsulting/jupyterlab-gswe-ki-ml-exercise:0.1.0"}"

cp -f "${SCRIPT_DIR}/../env.yaml" "${SCRIPT_DIR}/"
docker build -t "${IMAGE_TAGE}" -f "${SCRIPT_DIR}"/Containerfile "${SCRIPT_DIR}"

if [ "${1}" == "--push" ]; then
  docker push "${IMAGE_TAGE}"
fi