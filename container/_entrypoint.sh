#!/usr/bin/env bash

source /usr/local/bin/_conda_init.sh

if [ ! -f "${HOME}/.bashrc" ]; then
  cp /etc/skel/.bashrc "${HOME}/.bashrc" && chown "$(id -u):$(id -g)" "${HOME}/.bashrc"
fi
if [ ! -f "${HOME}/.bash_profile" ]; then
  cp /etc/skel/.bash_profile "${HOME}/.bash_profile" && chown "$(id -u):$(id -g)" "${HOME}/.bash_profile"
fi

exec "$@"