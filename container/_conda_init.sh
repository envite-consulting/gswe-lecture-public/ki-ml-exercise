#!/usr/bin/env bash

__conda_setup="$("${MAMBA_ROOT_PREFIX}/bin/conda" 'shell.bash' 'hook' 2> /dev/null)"
if [ -n "${__conda_setup}" ]; then
    eval "$__conda_setup"
else
    if [ -f "${MAMBA_ROOT_PREFIX}/etc/profile.d/conda.sh" ]; then
        . "${MAMBA_ROOT_PREFIX}/etc/profile.d/conda.sh"
    else
        export PATH="${MAMBA_ROOT_PREFIX}/bin:$PATH"
    fi
fi
unset __conda_setup

if [ -f "${MAMBA_ROOT_PREFIX}/etc/profile.d/mamba.sh" ]; then
    . "${MAMBA_ROOT_PREFIX}/etc/profile.d/mamba.sh"
fi

mamba activate "${ENV_NAME:-"base"}"