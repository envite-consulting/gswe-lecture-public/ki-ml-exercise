# Exercise KI & ML

Please open the notebook [gswe-exercise-8.ipynb](gswe-exercise-8.ipynb) to do the exercise.

The following sections are not required for the exercise. They only describe how you can do the exercise on your local machine if you want to do it again at home.

## Exercise on your local machine

Important: You can start Jupyter Lab on your local machine and do the exercise on Linux, Mac and Windows. However, measuring the actual energy consumption of the CPU only works on Linux with this implementation.

Since Linux kernel 5.4.0 _Powercap_ attributes are only accessible by _root_. This has been changed due to the security vulnerability [CVE-2020-8694](https://www.cve.org/CVERecord?id=CVE-2020-8694).
It has been discovered by [Platypus](https://platypusattack.com/) that RAPL can be used for software-based power side-channel attacks. This even allows to extract complete cryptographic keys based on the CPU power consumption.

With the following command you can enable temporary read access to the _RAPL_ energy counters via the _Powecap_ interface (automatically reverted after reboot:

```bash
sudo find /sys/ -name energy_uj -exec chmod 0444 {} \;
```

If you want to disable it again after the exercise just run the following command:

```bash
sudo find /sys/ -name energy_uj -exec chmod 0400 {} \;
```

### With Conda

First create a new _Conda_ environment, which contains all the required dependencies. You can do this with _Conda_ or any compatible tool. 
We used _micromamba_ which is a lightweight implementation. You can just download a single binary for your OS at <https://github.com/mamba-org/micromamba-releases/releases>.  

.Create the environment
```bash
micromamba create -f env.yaml
```

.Activate the environment
```bash
micromamba activate gswe-ki-ml-exercise
```

.Start Jupyter Lab
```bash
jupyter lab --notebook-dir="$(pwd)" --preferred-dir="$(pwd)" --NotebookApp.allow_origin='*' --NotebookApp.token=''
```

_Jupyter Lab_ should open automatically in your browser. If not, navigate to <http://localhost:8888/lab>.

### With Docker

.Start Jupyter Lab
```bash
docker run --name jupyterlab-gswe-ki-ml-exercise --rm \
  -v "$(pwd):/home/jovyan/work" \
  -v /proc/cpuinfo:/proc/cpuinfo:ro -v /sys:/sys:ro --privileged \
  -p 8888:8888 \
  enviteconsulting/jupyterlab-gswe-ki-ml-exercise:0.1.0 \
  jupyter lab --notebook-dir="/home/jovyan/work" --preferred-dir="/home/jovyan/work" --NotebookApp.allow_origin='*' --NotebookApp.token='' --ip 0.0.0.0
```

To open _Jupyter Lab_ navigate to <http://localhost:8888/lab> in your browser.

Instead of pulling from _Docker Hub_, you can also build the image.

.Build the Docker image
```bash
container/build.sh
```

.Build and publish the Docker image
```bash
container/build.sh --push
```

Hint: You can set the image name with the environment variable `IMAGE_TAG`.

## Carbon footprint dashboard on your local machine

### With Conda

First create a new _Conda_ environment, which contains all the required dependencies. You can do this with _Conda_ or any compatible tool. 
We used _micromamba_ which is a lightweight implementation. You can just download a single binary for your OS at <https://github.com/mamba-org/micromamba-releases/releases>.  

.Create the environment
```bash
micromamba create -f env-carbonboard.yaml
```

.Activate the environment
```bash
micromamba activate carbonboard
```

.Start the Carbon footprint dashboard
```bash
carbonboard --filepath=emissions-example.csv
```

To open the Carbon footprint dashboard open the Url <http://localhost:8050> in your browser.